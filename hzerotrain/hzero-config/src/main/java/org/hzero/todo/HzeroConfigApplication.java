package org.hzero.todo;

import org.hzero.autoconfigure.config.EnableHZeroConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@EnableHZeroConfig
@SpringBootApplication
public class HzeroConfigApplication {

	public static void main(String[] args) {
		SpringApplication.run(HzeroConfigApplication.class, args);
	}

}
