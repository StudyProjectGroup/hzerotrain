package com.wp.hzero;

import io.choerodon.swagger.annotation.Permission;
import io.swagger.annotations.ApiOperation;
import org.hzero.autoconfigure.platform.EnableHZeroPlatform;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;


//@EnableChoerodonResourceServer
@EnableHZeroPlatform            //开启hzero平台服务
@EnableDiscoveryClient
@SpringBootApplication
public class DemoPlatformApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoPlatformApplication.class, args);
    }

    @ApiOperation("测试测试")
    @Permission(permissionPublic = true)
    @GetMapping("/send-email")
    public void sendEmail() {

    }

}


