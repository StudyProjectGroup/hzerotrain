package com.wp.hzero.infra.mapper;

import io.choerodon.mybatis.common.BaseMapper;
import com.wp.hzero.domain.entity.Example;

/**
 * Mapper
 */
public interface ExampleMapper extends BaseMapper<Example> {

}
