package com.wp.hzero.app.service;

import com.baidu.unbiz.fluentvalidator.ValidatorContext;
import com.baidu.unbiz.fluentvalidator.ValidatorHandler;
import org.hzero.boot.imported.infra.validator.annotation.ImportValidator;
import org.hzero.boot.imported.infra.validator.annotation.ImportValidators;

/**
 * description
 *  自定义数据校验
 * @author shuangfei.zhu@hand-china.com 2019/07/21 14:46
 */
@ImportValidators({
        @ImportValidator(templateCode = "DEMO-IMPORT")
})
public class ValidateDemo extends ValidatorHandler<String> {

    @Override
    public boolean validate(ValidatorContext context, String data) {
        System.out.println("数据校验：" + data);
        // do something
        return true;
    }
}
