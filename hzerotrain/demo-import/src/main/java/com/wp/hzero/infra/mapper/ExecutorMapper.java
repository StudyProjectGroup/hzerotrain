package com.wp.hzero.infra.mapper;

import io.choerodon.mybatis.common.BaseMapper;
import com.wp.hzero.domain.entity.Executor;

/**
 * Mapper
 *
 * @author shuangfei.zhu@hand-china.com 2019-01-09 14:45:13
 */
public interface ExecutorMapper extends BaseMapper<Executor> {


}
