package com.wp.hzero.app.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wp.hzero.domain.entity.Executor;
import com.wp.hzero.domain.repository.ExecutorRepository;
import org.hzero.boot.imported.app.service.IDoImportService;
import org.hzero.boot.imported.infra.validator.annotation.ImportService;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.Map;

/**
 * description
 *  自定义导入
 * @author 文攀 2019/08/02 16:16
 */
@ImportService(templateCode = "DEMO-IMPORT")       //导入模板的编码
public class ImportDemo  implements IDoImportService{

    @Autowired
    ObjectMapper objectMapper;
    @Autowired
    ExecutorRepository executorRepository;


    @Override
    public Boolean doImport(String data) {
        Executor executor;
        try {
            executor = objectMapper.readValue(data, Executor.class);
        } catch (IOException e) {
            // 失败
            return false;
        }
        //进行数据导入
        executorRepository.insertSelective(executor);
        // 成功
        return true;
    }
}
