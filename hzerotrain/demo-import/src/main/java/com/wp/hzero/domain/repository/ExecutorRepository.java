package com.wp.hzero.domain.repository;

import com.wp.hzero.domain.entity.Executor;
import org.hzero.mybatis.base.BaseRepository;

/**
 * 资源库
 *
 * @author shuangfei.zhu@hand-china.com 2019-01-09 14:45:13
 */
public interface ExecutorRepository extends BaseRepository<Executor> {

}
