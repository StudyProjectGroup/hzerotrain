package org.hzero.todo.infra.mapper;

import io.choerodon.mybatis.common.BaseMapper;
import org.hzero.todo.domain.entity.User;

/**
 * Created by 文攀 on 2019/7/30.
 * 继承basemapper基本的CRUD都有
 */
public interface UserMapper extends BaseMapper<User> {
}
