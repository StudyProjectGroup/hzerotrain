package org.hzero.todo.domain.repository;

import io.choerodon.core.domain.Page;
import io.choerodon.mybatis.pagehelper.domain.PageRequest;
import org.hzero.mybatis.base.BaseRepository;
import org.hzero.todo.domain.entity.Task;

import java.util.List;

/**
 * 任务资源库
 */
public interface  TaskRepository  extends BaseRepository<Task> {
    List<Task> selectByEmployeeId(Long employeeId);

    Task selectDetailByTaskNumber(String taskNumber);

    Page<Task> pageTask(Task task, PageRequest pageRequest);

}
