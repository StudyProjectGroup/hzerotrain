package com.wp.hzero;

import org.hzero.autoconfigure.message.EnableHZeroMessage;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import io.choerodon.resource.annoation.EnableChoerodonResourceServer;

@EnableChoerodonResourceServer
@EnableHZeroMessage             //开启消息服务
@EnableDiscoveryClient
@SpringBootApplication
public class DemoMessageApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoMessageApplication.class, args);
    }
}


