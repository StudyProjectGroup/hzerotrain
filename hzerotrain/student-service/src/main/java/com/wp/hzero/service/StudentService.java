package com.wp.hzero.service;

import com.wp.hzero.entity.Course;
import com.wp.hzero.entity.Student;
import com.wp.hzero.entity.StudentVO;

import java.util.List;

/**
 * 学生业务service
 *
 * @author sitao.chao@hand-china.com 2019-07-21
 */
public interface StudentService {

    /**
     * 分页及按参数查询学生
     * @param student   入参
     * @param page  当前页数
     * @return  学生集合
     */
    List<Student> selectByPage(Student student, int page);

    /**
     * 根据学号查询当前学生选修的课程
     * @param studentNo 学号
     * @return  学生,课程
     */
    StudentVO selectByStudentNo(String studentNo);

    /**
     * 根据学号查询当前学生
     * @param studentNo 学号
     * @return  学生
     */
    Student selectOneByStudentNo(String studentNo);

    /**
     * 新建
     * @param course   课程
     */
    void aopInsertCourse(Course course);

    /**
     * 更新数据
     * @param student   学生
     * @return  学生
     */
    Student aopUpdateStudent(Student student);
}
