package com.wp.hzero.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.service.Tag;
import springfox.documentation.spring.web.plugins.Docket;

/**
 * Created by 文攀 on 2019/7/30.
 */
@Configuration
public class SwaggerApiConfig {

    public static final String STUDNET = "Student";

    @Autowired
    public SwaggerApiConfig(Docket docket) {
        docket.tags(
                new Tag(STUDNET, "学生信息")
        );
    }
}
