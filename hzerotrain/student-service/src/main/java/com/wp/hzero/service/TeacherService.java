package com.wp.hzero.service;

import com.wp.hzero.entity.TeacherVO;

import java.util.List;

/**
 * 老师service
 *
 * @author sitao.chao@hand-china.com 2019-07-22
 */
public interface TeacherService {

    /**
     * 老师结构树
     * @return  结构树
     */
    List<TeacherVO> tree();
}
