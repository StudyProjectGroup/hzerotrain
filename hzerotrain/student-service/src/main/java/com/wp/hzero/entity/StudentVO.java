package com.wp.hzero.entity;

import lombok.Data;

import java.util.List;

/**
 * 学生实体VO
 *
 * @author sitao.chao@hand-china.com 2019-07-22
 */
@Data
public class StudentVO {
    private String studentNo;
    private String studentName;
    private List<CourseVO> courseList;
}
