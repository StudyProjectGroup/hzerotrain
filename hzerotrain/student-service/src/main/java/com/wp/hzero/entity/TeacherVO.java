package com.wp.hzero.entity;

import lombok.Data;

import java.util.List;

/**
 * 老师实体VO
 *
 * @author sitao.chao@hand-china.com 2019-07-22
 */
@Data
public class TeacherVO {
    private String teacherNo;
    private String teacherName;
    List<TeacherVO> children;

    public TeacherVO setTeacherNo(String teacherNo) {
        this.teacherNo = teacherNo;
        return this;
    }

    public TeacherVO setTeacherName(String teacherName) {
        this.teacherName = teacherName;
        return this;
    }

    public TeacherVO setChildren(List<TeacherVO> children) {
        this.children = children;
        return this;
    }
}
