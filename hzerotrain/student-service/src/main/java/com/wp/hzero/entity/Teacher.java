package com.wp.hzero.entity;

import lombok.Data;

/**
 * 老师实体
 *
 * @author sitao.chao@hand-china.com 2019-07-22
 */
@Data
public class Teacher {
    private String teacherNo;
    private String teacherName;
    private String managerNo;
}
