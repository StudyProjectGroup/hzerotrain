package com.wp.hzero.service.impl;

import com.wp.hzero.entity.Teacher;
import com.wp.hzero.entity.TeacherVO;
import com.wp.hzero.mapper.TeacherMapper;
import com.wp.hzero.service.TeacherService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 老师service实现
 *
 * @author sitao.chao@hand-china.com 2019-07-22
 */
@Service
public class TeacherServiceImpl implements TeacherService {

    private final TeacherMapper teacherMapper;

    public TeacherServiceImpl(TeacherMapper teacherMapper) {
        this.teacherMapper = teacherMapper;
    }

    @Override
    public List<TeacherVO> tree() {
        List<Teacher> teacherList = teacherMapper.selectAll();
        // 根节点
        List<TeacherVO> rootTeacher = new ArrayList<>();
        for (Teacher nav : teacherList) {
            if(nav.getManagerNo() == null || "".equals(nav.getManagerNo())){//父节点是null的，为根节点。
                TeacherVO teacherVO = new TeacherVO().setTeacherNo(nav.getTeacherNo()).setTeacherName(nav.getTeacherName());
                rootTeacher.add(teacherVO);
            }
        }
        // 为根老师设置子老师，getChild是递归调用的
        for (TeacherVO nav : rootTeacher) {
            /* 获取根节点下的所有子节点 使用getChild方法*/
            List<TeacherVO> childList = getChild(nav.getTeacherNo(), teacherList);
            nav.setChildren(childList);//给根节点设置子节点
        }
        // 输出构建好的树结构
        return rootTeacher;
    }

    /**
     * 获取子节点
     * @param teacherNo 父节点id
     * @param allTeacher 所有老师
     * @return 每个根节点下，所有子老师列表
     */
    private List<TeacherVO> getChild(String teacherNo, List<Teacher> allTeacher){
        // 子菜单
        List<TeacherVO> childList = new ArrayList<>();
        for (Teacher nav : allTeacher) {
            // 遍历所有节点，将所有老师的父no与传过来的根节点的no比较
            // 相等说明：为该根节点的子节点。
            if(nav.getManagerNo() != null && nav.getManagerNo().equals(teacherNo)){
                TeacherVO teacherVO = new TeacherVO().setTeacherNo(nav.getTeacherNo()).setTeacherName(nav.getTeacherName());
                childList.add(teacherVO);
            }
        }
        // 递归
        for (TeacherVO nav : childList) {
            nav.setChildren(getChild(nav.getTeacherNo(), allTeacher));
        }
        // 如果节点下没有子节点，返回一个空List（递归退出）
        if(childList.size() == 0){
            return new ArrayList<>();
        }
        return childList;
    }
}
