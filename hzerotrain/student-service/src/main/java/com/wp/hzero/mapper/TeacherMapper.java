package com.wp.hzero.mapper;

import com.wp.hzero.entity.Teacher;

import java.util.List;

/**
 * 老师数据mapper
 *
 * @author sitao.chao@hand-china.com 2019-07-22
 */
public interface TeacherMapper {
    /**
     * 查询所有老师
     * @return  老师集合
     */
    List<Teacher> selectAll();
}
