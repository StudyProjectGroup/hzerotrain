package com.wp.hzero.controller;

import com.wp.hzero.entity.TeacherVO;
import com.wp.hzero.service.TeacherService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 老师控制器
 *
 * @author sitao.chao@hand-china.com 2019-07-22
 */
@RestController
public class TeacherController {

    private final TeacherService teacherService;

    public TeacherController(TeacherService teacherService) {
        this.teacherService = teacherService;
    }

    /**
     * 第八题,老师树结构
     *
     * @author sitao.chao@hand-china.com 2019-07-22
     */
    @RequestMapping("/teacher/tree")
    public List<TeacherVO> tree() {
        return teacherService.tree();
    }
}
