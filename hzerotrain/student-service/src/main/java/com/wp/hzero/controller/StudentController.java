package com.wp.hzero.controller;

import com.wp.hzero.config.SwaggerApiConfig;
import com.wp.hzero.entity.Course;
import com.wp.hzero.entity.Student;
import com.wp.hzero.entity.StudentVO;
import com.wp.hzero.service.StudentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * 学生控制器
 *
 * @author sitao.chao@hand-china.com 2019-07-22
 */
@RestController
//@Api(tags = "Student学生服务")
@Api(tags = SwaggerApiConfig.STUDNET)
@RequestMapping("/student")
public class StudentController {
    private static final String DEFAULT_PAGE = "1";

    //private final StudentService studentService;
    @Autowired
    StudentService studentService;

    /*public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }*/

    /**
     * 第一题分页按参数查询学生数据
     * @param student   入参
     * @param page  当前页数
     * @return  学生集合
     */
    @ApiOperation(value = "分页，按参数查询学生数据")
    @GetMapping
    @ResponseBody
    public List<Student> selectByPage(Student student,
                                      @RequestParam(value = "page",defaultValue = DEFAULT_PAGE) int page) {
        return studentService.selectByPage(student, page);
    }

    /**
     * 第二题,根据学号查询当前学生选修的课程
     * @param studentNo 学号
     * @return  学生,课程集合
     */
    @ApiOperation(value = "根据学号查询当前学生选修的课程")
    @GetMapping("/{studentNo}")
    @ResponseBody
    public StudentVO selectByStudentNo(@PathVariable String studentNo) {
        return studentService.selectByStudentNo(studentNo);
    }

    /**
     * 第三题,新建数据
     * @param course   课程
     * @return  新建的数据
     */
    @ApiOperation(value = "新建学生数据")
    @PostMapping
    @ResponseBody
    public Course insertCourse(@RequestBody Course course) {
        studentService.aopInsertCourse(course);
        return course;
    }

    /**
     * 第四题,更新数据
     * @param student   学生
     * @return  学生
     */
    @ApiOperation(value = "更新学生数据")
    @PutMapping
    @ResponseBody
    public Student updateStudent(@RequestBody Student student) {
        return studentService.aopUpdateStudent(student);
    }

    /**
     * 第六题,将当前学⽣的数据放⼊SESSION中
     * @param studentNo 学号
     * @return  学生
     */
    @GetMapping("/session/{studentNo}")
    @ResponseBody
    public Boolean studentSession(@PathVariable String studentNo, HttpSession httpSession) {
        Student student = studentService.selectOneByStudentNo(studentNo);
        httpSession.setAttribute("student", student);
        return true;
    }

    /**
     * 第七题,将从session获取学⽣的数据
     * @return  学生
     */
    @GetMapping("/session/get")
    @ResponseBody
    public Student  getStudentSession(HttpSession httpSession) {
        return (Student) httpSession.getAttribute("student");
    }
}
