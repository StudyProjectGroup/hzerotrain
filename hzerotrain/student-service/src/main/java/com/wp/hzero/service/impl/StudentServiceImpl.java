package com.wp.hzero.service.impl;


import com.wp.hzero.entity.Course;
import com.wp.hzero.entity.Student;
import com.wp.hzero.entity.StudentVO;
import com.wp.hzero.mapper.StudentMapper;
import com.wp.hzero.service.StudentService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 学生service实现
 *
 * @author sitao.chao@hand-china.com 2019-07-22
 */
@Service
@Transactional
public class StudentServiceImpl implements StudentService {

    private final StudentMapper studentMapper;

    public StudentServiceImpl(StudentMapper studentMapper) {
        this.studentMapper = studentMapper;
    }

    @Override
    public List<Student> selectByPage(Student student, int page) {
        int sqlPage = page*5 - 5;
        return studentMapper.selectByPage(student, sqlPage);
    }

    @Override
    public StudentVO selectByStudentNo(String studentNo) {
        return studentMapper.selectByStudentNo(studentNo);
    }

    @Override
    public Student selectOneByStudentNo(String studentNo) {
        return studentMapper.selectOneByStudentNo(studentNo);
    }

    @Override
    @Transactional
    public void aopInsertCourse(Course course) {
        studentMapper.insertCourse(course);
    }

    @Override
    public Student aopUpdateStudent(Student student) {
        boolean updateFlag = student.getStudentNo() != null
                && student.getStudentName() == null
                && student.getStudentGender() == null
                && student.getStudentAge() == null;
        if(updateFlag) {
            System.out.println("当前数据⽆法进⾏更新");
            //throw new CustomerException("当前数据⽆法进⾏更新");
        }
        int count = studentMapper.updateStudent(student);
        if (count == 0) {
            System.out.println("版本号与数据不⼀致");
            //throw new CustomerException("版本号与数据不⼀致");
        }
        return student;
    }
}
