package com.wp.hzero.entity;

import lombok.Data;

import java.util.Date;

/**
 * 课程实体
 *
 * @author sitao.chao@hand-china.com 2019-07-22
 */
@Data
public class Course {
    private Long id;
    private String courseNo;
    private String courseName;
    private String teacherNo;
    private Long objectVersionNumber;
    private Date creationDate;
    private Long createdBy;
    private Long lastUpdatedBy;
    private Date lastUpdateDate;
    private Long lastUpdateLogin;
}
