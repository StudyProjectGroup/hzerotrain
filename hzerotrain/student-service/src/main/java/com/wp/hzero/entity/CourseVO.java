package com.wp.hzero.entity;

import lombok.Data;

/**
 * 课程实体VO
 *
 * @author sitao.chao@hand-china.com 2019-07-22
 */
@Data
public class CourseVO {
    String courseNo;
    String courseName;
}
