package com.wp.hzero.mapper;

import com.wp.hzero.entity.Course;
import com.wp.hzero.entity.Student;
import com.wp.hzero.entity.StudentVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 学生数据mapper
 *
 * @author sitao.chao@hand-china.com 2019-07-21
 */
public interface StudentMapper {

    /**
     * 分页及按参数查询学生
     * @param student   入参
     * @param page  当前页数
     * @return  学生集合
     */
    List<Student> selectByPage(@Param("student") Student student, @Param("page") int page);

    /**
     * 根据学号查询当前学生选修的课程
     * @param studentNo 学号
     * @return  学生,课程
     */
    StudentVO selectByStudentNo(@Param("studentNo") String studentNo);

    /**
     * 新建
     * @param course   课程
     */
    void insertCourse(Course course);

    /**
     * 更新
     * @param student   学生
     */
    int updateStudent(Student student);

    /**
     * 根据学号查询当前学生
     * @param studentNo 学号
     * @return  学生
     */
    Student selectOneByStudentNo(String studentNo);
}
