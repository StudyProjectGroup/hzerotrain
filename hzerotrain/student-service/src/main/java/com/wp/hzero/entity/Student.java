package com.wp.hzero.entity;

import lombok.Data;

import java.util.Date;

/**
 * 学生实体
 *
 * @author sitao.chao@hand-china.com 2019-07-22
 */
@Data
public class Student {
    private Long id;
    private String studentNo;
    private String studentName;
    private Long studentAge;
    private String studentGender;
    private Long objectVersionNumber;
    private Date creationDate;
    private Long createdBy;
    private Long lastUpdatedBy;
    private Date lastUpdateDate;
    private Long lastUpdateLogin;
}
