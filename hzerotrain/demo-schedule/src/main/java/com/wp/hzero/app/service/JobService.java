package com.wp.hzero.app.service;

import org.hzero.boot.scheduler.infra.annotation.JobHandler;
import org.hzero.boot.scheduler.infra.enums.ReturnT;
import org.hzero.boot.scheduler.infra.handler.IJobHandler;
import org.hzero.boot.scheduler.infra.tool.SchedulerTool;

import java.util.Map;

/**
 * description
 *
 * @author shuangfei.zhu@hand-china.com 2019/07/21 16:02
 */
@JobHandler("demo")
public class JobService implements IJobHandler {

    /**
     * 该方法是调度作业的具体方法
     * @param map
     * @param tool
     * @return
     */
    @Override
    public ReturnT execute(Map<String, String> map, SchedulerTool tool) {
        String name = map.get("name");
        System.out.println("hello " + name);
        return ReturnT.SUCCESS;
    }
}
