package org.hzero.todo;

import org.hzero.autoconfigure.register.EnableHZeroRegister;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@EnableHZeroRegister
@SpringBootApplication
public class HzeroRegisterApplication {

	public static void main(String[] args) {
		SpringApplication.run(HzeroRegisterApplication.class, args);
	}

}
