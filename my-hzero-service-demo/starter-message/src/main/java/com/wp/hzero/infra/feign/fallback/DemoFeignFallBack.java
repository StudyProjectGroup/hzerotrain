package com.wp.hzero.infra.feign.fallback;

import com.wp.hzero.infra.feign.DemoFeign;
import org.springframework.stereotype.Component;

/**
 * DemoFeignFallBack
 */
@Component
public class DemoFeignFallBack implements DemoFeign {

}
