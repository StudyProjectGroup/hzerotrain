package com.wp.hzero.domain.repository;

import com.wp.hzero.domain.entity.Example;
import org.hzero.mybatis.base.BaseRepository;

/**
 * Repository
 */
public interface ExampleRepository extends BaseRepository<Example> {

}
