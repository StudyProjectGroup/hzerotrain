package com.cmy.infra.feign.fallback;

import com.cmy.infra.feign.DemoFeign;
import org.springframework.stereotype.Component;

/**
 * DemoFeignFallBack
 */
@Component
public class DemoFeignFallBack implements DemoFeign {

}
