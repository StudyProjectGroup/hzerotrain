package com.cmy.domain.repository;

import com.cmy.domain.entity.Example;
import org.hzero.mybatis.base.BaseRepository;

/**
 * Repository
 */
public interface ExampleRepository extends BaseRepository<Example> {

}
