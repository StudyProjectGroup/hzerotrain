package com.cmy.infra.mapper;

import io.choerodon.mybatis.common.BaseMapper;
import com.cmy.domain.entity.Example;

/**
 * Mapper
 */
public interface ExampleMapper extends BaseMapper<Example> {

}
